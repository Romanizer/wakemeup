# WakeMeUp

An App for the Raspberry Pi to act like an alarm clock with superpowers.

This uses a TI Class-D Amplifier that is controlled by the RPi to play music to wake your lazy ass up.

# New Features

* set alarms for specific weekdays/dates at specific times
* alarms can shuffle play specific artists/albums from a list
* shuffle play has a global timelimit
* play of specific songs
* wildcards allowed for testing (**:**)
* web interface has button to disable current alarms
* A small audio file (wakeline) is played before each alarm
* volume controllable on web interface
* digital potentiometer for audio? addressable over i2c/spi/...
* alarm for specific date self destructs (not needed after date out of date)
* alarm templates for faster alarm creation
* addressable ARGB LEDs using dmxvis

# Pinout

|Pin|GPIO|Function|
|-|-|-|
|7|GPIO4|Mute, ON-LED|
|29|GPIO5|Shutdown|

To activate Amp on wakeboardv3:

* pull GPIO5/29 LOW
* small delay
* pull GPIO4/7  HIGH

To deactivate, reverse order and reverse pin states.

# Dependencies

```sh
apt install pkg-config libasound2-dev
```

# History

This all started back in 2017, with a small Java program, 
which would connect to a database and start playing alarms.

The first Hardware generation did not have a software controlled shutdown,
the speakers were running 24/7.

Just a Year later I rewrote the entire service in python,
which offered direct gpio control on the RPi.

With this came a new (not home etched) PCB, now giving control of the TI Amplifier to the RPi.

At some point, I made an Android app as well. It could toggle alarms, create new ones and modify old alarms.


Currently the entire project is to be rewritten in rust.

The goal is to make a web interface that can control not only scheduled alarms, but currently running ones as well.
