/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ["./templates/*.html", "./static/*.html"],
  theme: {
    extend: {},
  },
  plugins: [],
}

