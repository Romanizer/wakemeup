
#[derive(Default, PartialEq, Clone)]
pub enum State {
    #[default]
    Idle,
    Playing,
    Check,
}

pub enum Command {
    AlarmFound,
    Over,
    Stop,
}

pub struct Machine {
    state: State,
    last_state: State,
}

impl Machine {
    pub fn new() -> Self {
        Machine {
            state: State::default(),
            last_state: State::default(),
        }
    }

    // maybe return new state? if new state reached?
    pub fn update(&mut self, command: Command) {
        // define the state transitions here
        let new_state = match (&self.state, command) {
            (State::Idle, Command::AlarmFound) => Some(State::Playing),
            (State::Playing, Command::Stop) => Some(State::Idle),
            (State::Playing, Command::Over) => Some(State::Idle),
            (_, _) => None,
        };

        // finally, transition to new state
        match new_state {
            Some(x) => {
                self.last_state = self.state.clone();
                self.state = x;
            },
            None => {/* no-op */},
        }
    }

    pub fn is_state(&self, state: State) -> bool {
        self.state == state
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn is_state() {
        let fsm = Machine::new();
        assert!(fsm.is_state(State::Idle));
        assert!(!fsm.is_state(State::Playing));
    }

    #[test]
    fn update() {
        let mut fsm = Machine::new();
        fsm.update(Command::AlarmFound);
        assert!(fsm.is_state(State::Playing));
        fsm.update(Command::AlarmFound);
        assert!(fsm.is_state(State::Playing));

        fsm.update(Command::Over);
        assert!(fsm.is_state(State::Idle));
    }
}
