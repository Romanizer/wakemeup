use std::sync::{Arc, Mutex};
use std::thread;

use rppal::gpio::{Gpio, OutputPin};
use rppal::system::DeviceInfo;

// Gpio uses BCM pin numbering. BCM GPIO 23 is tied to physical pin 16.
const GPIO_LED: u8 = 14;

// Mutes output on TI Amp, when low
const GPIO_MUTE: u8 = 4;

// Puts TI Amp into Sleep mode, when high
const GPIO_SHUTDOWN: u8 = 5;

pub type Pin = Option<Arc<Mutex<OutputPin>>>;

pub fn create_gpio() -> (Pin, Pin) {
    // setup GPIO
    let mut gpio_mute = None;
    match Gpio::new() {
        Ok(e) => {
            println!("[GPIO] {}", DeviceInfo::new().unwrap().model());
            let pin = e.get(GPIO_MUTE).unwrap().into_output();
            gpio_mute = Some(Arc::new(Mutex::new(pin)));
        },
        Err(e) => {
            println!("[GPIO] Not Using GPIO: {:?}", e);
        },
    }

    let mut gpio_shutdown = None;
    match Gpio::new() {
        Ok(e) => {
            println!("[GPIO] {}", DeviceInfo::new().unwrap().model());
            let pin = e.get(GPIO_SHUTDOWN).unwrap().into_output();
            gpio_shutdown = Some(Arc::new(Mutex::new(pin)));
        },
        Err(e) => {
            println!("[GPIO] Not Using GPIO: {:?}", e);
        },
    }

    (gpio_mute, gpio_shutdown)
}

pub fn toggle(state: bool, mute: Pin, shutdown: Pin) {
    match (mute, shutdown) {
        (Some(gpio_mute), Some(gpio_shutdown)) => {
            let mut pin_mute = gpio_mute.lock().unwrap();
            let mut pin_shutdown = gpio_shutdown.lock().unwrap();
            if state {
                println!("[GPIO] Amplifier turning on");
                pin_shutdown.set_low();
                thread::sleep(std::time::Duration::from_millis(10));
                pin_mute.set_high();
            } else {
                println!("[GPIO] Amplifier shutting down");
                pin_mute.set_low();
                thread::sleep(std::time::Duration::from_millis(10));
                pin_shutdown.set_high();
            }
        },
        (_, _) => {
            println!("[GPIO] Dummy was toggled");
        },
    }
}
