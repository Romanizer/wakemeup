use std::sync::{ Arc, Mutex };

use axum::{
    extract::State,
    routing::{get, get_service, post},
    response::{Html, IntoResponse},
    http::StatusCode,
    Json, Router,
    Form,
};

use serde::Deserialize;

use tower_http::services::ServeDir;

use askama::Template;

use std::net::SocketAddr;

use rodio::{ Decoder, OutputStream, OutputStreamHandle, Sink, source::Source };

use sqlx::{ sqlite::SqlitePool, query, query_as, FromRow };

mod gpio;
use gpio::create_gpio;

#[cfg(debug_assertions)]
use std::process::Command;

const DB: &str = ".db";

#[derive(Clone)]
struct AppState {
    led_on: Arc<Mutex<bool>>,
    gpio_mute: gpio::Pin,
    gpio_shutdown: gpio::Pin,
    stream: Arc<Mutex<Sink>>,
    pool: Arc<Mutex<SqlitePool>>,
}

#[derive(Deserialize, Debug, Template, FromRow)]
#[template(path = "alarm.html")]
struct Alarm {
    id: i64,
    name: String,
    enabled: bool,
    time: String,
    day: Option<String>,
    date: Option<String>,
    artist: Option<String>,
    music: Option<String>,
}

#[derive(Template)]
#[template(path = "alarm_list.html", escape = "none")]
pub struct AlarmList {
    alarms: Vec<Alarm>,
}

#[derive(Template)]
#[template(path = "editor.html")]
pub struct EditAlarm<'a> {
    alarm: Option<&'a Alarm>,
}

#[tokio::main]
async fn main() {
    let pool = SqlitePool::connect(DB).await.unwrap();
    let pool = Arc::new(Mutex::new(pool));

    let led_on = Arc::new(Mutex::new(false));

    let (_stream, stream_handle) = OutputStream::try_default().unwrap();
    let sink = Sink::try_new(&stream_handle).unwrap();
    sink.set_volume(0.25);
    let stream = Arc::new(Mutex::new(sink));

    let (gpio_mute, gpio_shutdown) = create_gpio();
    
    let app_state = AppState { led_on, gpio_mute, gpio_shutdown, stream, pool };

    // AXUM routes setup
    let app = Router::new()
        .route("/", get(alarm_list))
        .route("/edit", get(edit_alarm))
        .route("/submit", post(edit_alarm_data))
        .route("/newalarm", post(new_alarm))
        .route("/stop", post(toggle))
        .route("/delete", post(alarm_delete))
        // Fallback option is static routes that got no special handler (/)
        .fallback_service(routes_static())
        .with_state(app_state);

    let addr = SocketAddr::from(([0, 0, 0, 0], 3030));
    println!("[AXUM] listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

fn routes_static() -> Router {
    Router::new().nest_service("/", get_service(ServeDir::new("./static")))
}

fn rebuild_tailwind() {
    #[cfg(debug_assertions)]
    {
        println!("[DEBUG] Rebuilding Tailwind CSS...");
        Command::new("npx")
            .args(["tailwindcss", "-o", "./static/index.css"])
            .spawn().expect("Could not compile Tailwind CSS!");
    }
}

//#[axum::debug_handler]
async fn alarm_list(State(app_state): State<AppState>) -> impl IntoResponse {
    rebuild_tailwind();
    println!("[AXUM] reloading alarmlist template");

    // clone mutex so it instantly closes again
    // errors on non-cloned pool being passed into async fn
    let pool = app_state.pool.lock().unwrap().clone();

    let veclist = query_as!(Alarm, "SELECT * FROM wake")
        .fetch_all(&pool)
        .await.unwrap();

    let list = AlarmList { alarms: veclist };

    Html(list.render().unwrap())
}

// shall return html form with submit button to edit existing alarm
async fn edit_alarm(State(app_state): State<AppState>) -> impl IntoResponse {
    // TODO: differentiate between editing new alarm and existing (GET)
    rebuild_tailwind();
    let editor = EditAlarm { alarm: None };
    Html(editor.render().unwrap())
}

async fn edit_alarm_data(State(app_state): State<AppState>, Form(payload): Form<Alarm>) -> impl IntoResponse {
    println!("[AXUM] edited existing alarm");
    println!("{:?}", payload);
    ()
}

async fn new_alarm(State(app_state): State<AppState>, Json(payload): Json<Alarm>) -> StatusCode {
    println!("[AXUM] creating new alarm");
    println!("{:?}", payload);
    let pool = app_state.pool.lock().unwrap().clone();

    let query_str = r#"INSERT INTO wake (name,enabled,time,day,date,artist,music)
                    VALUES (?,?,?,?,?,?,?)"#;
    let q = query(query_str)
        .bind(payload.name)
        .bind(payload.enabled)
        .bind(payload.time)
        .bind(payload.day)
        .bind(payload.date)
        .bind(payload.artist)
        .bind(payload.music)
        .execute(&pool)
        .await;
    match q {
        Ok(x) => {
            println!("[SQLX] Inserted new Alarm {:?}", x);
            return StatusCode::OK;
        },
        Err(e) => {
            println!("[SQLX] Error on Insert: {:?}", e);
            return StatusCode::IM_A_TEAPOT;
        },
    }

}

async fn alarm_delete(State(app_state): State<AppState>, Json(payload): Json<i64>) -> StatusCode {
    println!("{:?}", payload);
    let pool = app_state.pool.lock().unwrap().clone();

    let query_str = r#"DELETE FROM wake WHERE id=?"#;

    let res = query(query_str).bind(payload).execute(&pool).await;

    match res {
        Ok(x) => {
            println!("[SQLX] Delete Completed of id={:?}\n{:?}", payload, x);
            StatusCode::OK
        },
        Err(e) => {
            println!("{:?}", e);
            StatusCode::IM_A_TEAPOT
        },
    }
}

async fn toggle(State(app_state): State<AppState>) {
    let stream = app_state.stream.lock();
    match stream {
        Ok(e) => {
            if ! e.empty() {
                println!("[RODIO] Stream currently on -> Stopping");
                // stop() instead of clear() as clear breaks playback
                e.stop();
                gpio::toggle(false, app_state.gpio_mute, app_state.gpio_shutdown);
            } else {
                gpio::toggle(true, app_state.gpio_mute, app_state.gpio_shutdown);
                println!("[RODIO] Stream currently paused -> Starting");
                let file = std::io::BufReader::new(std::fs::File::open("../local_audio/11 - L0V3 4RR0W 5H007 (Hmt2 Remix).flac").unwrap());
                //println!("{:?}", file);
                let source = Decoder::new(file).unwrap();
                e.append(source);
                e.play();
            }
        },
        Err(e) => {
            println!("[RODIO] {:?}", e);
        },
    }
}

