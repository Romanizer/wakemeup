
use std::process::Command;

macro_rules! p {
    ($($tokens: tt)*) => {
        println!("cargo:warning={}", format!($($tokens)*))
    }
}

fn main() {
    p!("Building Tailwind CSS...");
    Command::new("npx")
        .args(["tailwindcss", "-o", "./static/index.css"])
        .spawn().expect("Could not compile Tailwind CSS!");
}
