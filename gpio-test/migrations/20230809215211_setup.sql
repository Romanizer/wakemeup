-- Add up migration script here

CREATE TABLE IF NOT EXISTS wake (
    id integer primary key, 
    name text NOT NULL, 
    enabled boolean NOT NULL, 
    time text NOT NULL, 
    day text, 
    date text, 
    artist text, 
    music text
);
