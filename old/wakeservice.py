import config
import os
import mplayer
import time
# This class is a interface to MPlayer and the GPIO system calls
# this will simplify the Logic of the main.py file


class wakeservice:

    # internal
    update_interval = 10

    # States

    # mplayer
    shouldStop = False
    counter = 0

    def __init__(self):
        # load configuration here
        print("Loading Configuration...")
        config.config(self)
        # set up music player
        self.player = mplayer.mplayer(self.mute, self.mute_inv, self.shutdown, self.shutdown_inv)
    
    def update(self):
        # update states etc here
        print("updating")
        return
        if self.shouldStop == False:
            
            self.player.start(f"{self.lib_local}/honey.flac")#Wonder Why (prod. Robin).wav")
            self.shouldStop = True
        else:
            if self.counter < 5:
                self.counter += 1
                return
            self.counter = 0
            self.player.stop()
            self.shouldStop = False

    def checkAlarms(self):
        # Check if any alarm is active, return true
        print("Checking Alarms if active")
        return True

    def startAlarm(self, id):
        # Start alarm with id x
        print(f"Starting Alarm {id}")
        self.player.start(f"{self.lib_local}/honey.flac")#Wonder Why (prod. Robin).wav")

    def stopAlarm(self):
        # stop current Alarm if running
        print("Stopping Current Alarm")
        # for testing:
        self.player.stop()

   