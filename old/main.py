import wakeservice
import time
from flask import Flask, render_template, request
import threading

# Web server configuration

app = Flask(__name__, template_folder='web')

@app.route('/', methods = ['GET', 'POST'])
def serve():
    if request.method == 'POST':
        if request.form.get('button') == "stop":
            wake.stopAlarm()
        if request.form.get('button') == "play":
            wake.startAlarm(1)
    return render_template('index.html')


print('---##--- Starting Webserver ---##---')

# start Flask web server without reload (only available on main Thread)
threading.Thread(target=lambda: app.run(host="0.0.0.0", debug=True, use_reloader=False)).start()

# Wake Server Config

print('---##--- Wake Up Service V3 ---##---')
wake = wakeservice.wakeservice()
interval = wake.update_interval

while True:

    wake.update()
    time.sleep(interval/2)
    # see if music player is running
    print(wake.player.running())
    time.sleep(interval/2)
