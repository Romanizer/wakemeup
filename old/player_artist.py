import os, random, sys
import mutagen
import logging

givenwaketime = int(sys.argv[1]) # in seconds
artistdir = sys.argv[2] # full path to artist folder

logging.basicConfig(filename='wake_play_artist.log',level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%d.%m.%Y %H:%M:%S')  # totally wrog time (1 or 2 hours off)
# logging starting time to file:
logging.info("Alarm starting...")
logging.info(f"Dir: {artistdir}")

file_extensions = [".mp3", ".wav", ".ogg", ".flac", ".m4a"]

actualwaketime = 0

allfiles = list() # new list of all files
for (dirpath, dirnames, filenames) in os.walk(artistdir):
    allfiles += [os.path.join(dirpath, file) for file in filenames] # append all files with full path

songs = list() # list of actual songs to play
for file in allfiles:
    for ext in file_extensions:
        if ext in file.lower()[-6:]: # check if file is a music file
            songs.append(file)


while songs:  # while list of songs not empty, do:
    
    choosensong = random.choice(songs)
    songlength = mutagen.File(choosensong).info.length
    
    if songlength+actualwaketime <= givenwaketime:
        logging.info(f"Playing: {choosensong}")
        print(f"-------------")
        print(f"SONG PLAYING: {choosensong}")
        print(f"SONG   LENGTH: {int(songlength)} sec")
        print(f"PLAYLIST  LEN: {int(songlength+actualwaketime)} sec")
        print(f"TARGET LENGTH: {givenwaketime} sec")
        os.system(f"mplayer '{choosensong}' > /dev/null 2>&1")
        actualwaketime = actualwaketime + songlength
        songs.remove(choosensong) # remove song, because it was played
    else:
        logging.info(f"Skipping {choosensong}")
        songs.remove(choosensong) # remove song, because it doesnt fit in time budget

logging.info("Done Playing Music")
print("Done Playing Music")