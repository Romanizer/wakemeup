import os, random

print('---##--- Playing Random Wake-Line ---##---')

#dir lib music library
lib_remote = "/media/media/Musik/"
lib_local = '/home/goal/wake/local_audio/' 
lib_wakelines = '/home/goal/wake/wakelines/'

wakeline_to_play = random.choice(os.listdir(lib_wakelines)) #change dir name to whatever
print(f"wakeline: {wakeline_to_play}")

os.system("mplayer " + lib_wakelines + wakeline_to_play)