from configparser import ConfigParser
from os.path import exists

class config:
    def __init__(self, caller):
        # load config
        print("Loading Config")

        config = ConfigParser()
        # check if config.ini exists, if not create std config here
        if not exists("config.ini"):
            self.createStdConfig(config)
        config.sections()
        config.read("config.ini")
        # set settings
        caller.timezone = config.get('settings', 'timezone')
        caller.usingsqlite = config.get('settings', 'usingsqlite')
        caller.waketime = config.getint('settings', 'waketime')
        # set paths 
        caller.lib_local = config.get('paths', 'local')
        caller.lib_remote = config.get('paths', 'remote')
        caller.lib_wakelines = config.get('paths', 'wakelines')
        # set up gpio
        caller.mute = config.getint('gpio', 'mute')
        caller.mute_inv = config.getboolean('gpio', 'mute_inv')
        caller.shutdown = config.getint('gpio', 'shutdown')
        caller.shutdown_inv = config.getboolean('gpio', 'shutdown_inv')
        # set database config
        caller.db_addr = config.get('database', 'db_addr')
        caller.db_user = config.get('database', 'db_user')
        caller.db_pass = config.get('database', 'db_pass')
        caller.db_name = config.get('database', 'db_name')
        caller.db_tabl = config.get('database', 'db_tabl')
        caller.db_file = config.get('database', 'db_file')
    
    def createStdConfig(self, config):
        print("Configuration file config.ini not found. Creating standard configuration...")
        config['settings'] = {
            'Timezone': 'Europe/Berlin',
            'UsingSQLite': 'yes',
            'waketime': f'{60*50}' # in seconds
        }
        config['paths'] = {
            'remote': '/media/media/Musik/',
            'local': '/home/goal/wake/local_audio',
            'wakelines':'/home/goal/wake/wakelines/',
        }
        config['gpio'] = {
            'mute': '7',
            'mute_inv': 'no',
            'shutdown': '21',
            'shutdown_inv': 'yes'
        }
        config['database'] = {
            'db_addr': '',      # localhost / 192.168.x.x
            'db_user': '',      # your db user, the less priviledges the better
            'db_pass': '',
            'db_name': 'wake',  # name of database 
            'db_tabl': 'wake',
            'db_file': 'wake.db'
        }

        with open("config.ini", 'w') as f:
            config.write(f)
        return