import pymysql
import sqlite3
from subprocess import call
from datetime import datetime, time
import time
from time import strftime, gmtime
import os
import pytz
from pytz import timezone
import logging
import random

# 1 for SQLite3 DB - 0 for MySQL DB
usingSQLite = 1

print('---##--- Wake Up Service V3 ---##---')

# DB Config
db_addr = ""         # localhost / 192.168.x.x
db_user = ""         # your db user, the less priviledges the better
db_pass = ""
db_name = "wake"     # name of database 
db_tabl = "wake"     # name of table (also used for SQLite3)

# SQLite3
db_file = "wake.db"  # location of sqlite3 file/db

# setup gpio
# MUTE PIN
os.system("sudo gpio mode 7 out")
os.system("sudo gpio write 7 0")
# SHUTDOWN PIN
os.system("sudo gpio mode 21 out")
os.system("sudo gpio write 21 1")

utc = pytz.utc
localtime = timezone("Europe/Berlin")

# logging config
logging.basicConfig(filename='wake.log',level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%d.%m.%Y %H:%M:%S')  # totally wrong time (1 or 2 hours off)
# logging starting time to file:
logging.info("Wake Up Service starting...")

#dir lib music library
lib_remote = "/media/media/Musik/"
lib_local = '/home/goal/wake/local_audio/' 
lib_wakelines = '/home/goal/wake/wakelines/'

tru="1"
fals="0"

waketime = 60*50 #60*60 # 60 sec * 60 min = 1 hour    # time the music should play for

while True:
    
    # MUTE AGAIN TO AVOID *HISSS* on TI amp
    # SHUTDOWN PIN
    os.system("gpio mode 21 out")
    os.system("gpio write 21 1")
    # MUTE PIN
    os.system("gpio mode 7 out")
    os.system("gpio write 7 0")
    
    if(usingSQLite):
        try:
            conn = sqlite3.connect(db_file)
        except ValueError:
            logging.warning("ERROR connecting to Database (using SQLite3)")
            print("ERROR: SQLite3 connection issue")
        
        # SQLite3 specific cursor
        conn.row_factory = sqlite3.Row
        cur = 0
        cur = conn.cursor()
        cur.execute("SELECT * FROM " + db_tabl)
        
    else:
        # re-initialize mysql connection to refetch alarm list with maybe updated alarms
        try:
            time.sleep(60)
            conn = pymysql.connect(db_addr, db_user, db_pass, db_name)
            print("MySQL connection successful")
            logging.info("Successfully connected to Database")
            #logging.info("Connection to Database established")    # to much garbage data generation
        except ValueError:
            logging.warning("ERROR connecting to Database (MySQL)")
            print("ERROR: MySQL connection not established")
        
        # cursor for mysql selections
        cur = 0
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute("SELECT * FROM " + db_tabl)
        
        
    # alot of time related things, for Time and Day calc, to compare with individual alarms
    current_date = datetime.now(localtime)
    timenow = current_date.strftime('%H:%M')                                                            # Time     12:34
    daynow = current_date.strftime('%a')                                                                # Day     4         (Weekday: Wednesday)
    datenow = current_date.strftime('%d') + '.' + current_date.strftime('%m')                            # Date     12.03
    extendedTime = daynow + " " + datetime.now(localtime).strftime('%H:%M:%S') # ex: Sun 12:53:04
    
    # print out TIME, DATE....
    print(extendedTime)

    for row in cur:
        #  print("%s %s %s" % (row["TIME"], row["DAY"], row["ACTIVE"])) # debug thingy 
        if(row["Time"]==timenow or row["Time"]=="*"):                                                        # Is Alarm Time right?
            print("matched Time")
            if( daynow.lower() in str(row["Day"]).lower() or datenow in str(row["Date"]) or str(row["Day"])=="*"):        # Is Weekday right OR is Date right? is "Sun" in db?
                print("matched Day")
                if(row["Enabled"] == tru):                                            # Is alarm active?
                    
                    # print out current alarm starting
                    print("Alarm: %s" % row["ID"])
                    
                    os.system("gpio write 21 0")     # audio chip turns on
                    time.sleep(0.5)
                    os.system("gpio write 7 1")      # audio chip unmute
                    time.sleep(0.5)
                    
                    # play funny sound file i got from friends
                    os.system("python3 wake_lines.py")
                    
                    # decide between Local Music File OR remote Music Artist OR Internet Audio Stream
                    if("http:" in str(row["Music"])): # Remote Internet Radio Stream
                        logging.info("Starting Alarm: %s  (radio stream)" % str(row["Name"]))
                        os.system("mplayer " + str(row["Music"]))
                        
                    elif(str(row["Artist"]) == ""):  # Local Music File (prefer Artist over local music)
                        logging.info("Starting Alarm: %s  (local Music)" % str(row["Name"]))
                        os.system("mplayer " + lib_local + str(row["Music"]))
                        
                    else:                             # Remote Music Artist
                        artist_to_play = str(row["Artist"])
                        # randomly select an artist if there are multiple (comma seperated)
                        if ',' in artist_to_play:
                            artist_to_play = random.choice(artist_to_play.split(","))
                        # try to find artist in music library
                        logging.info("Starting Alarm: %s  (remote Artist)" % str(row["Name"]))
                        for dir in os.listdir(lib_remote):                                      #find artist in music lib
                            if(artist_to_play.lower() in dir.lower()):
                                os.system(f"python3 player_artist.py {waketime} '{lib_remote + dir}'")

                    os.system("gpio write 7 0")      # audio chip mute
                    time.sleep(0.5)
                    os.system("gpio write 21 1")     # audio chip shutdown
    # FOR row in cur END
    conn.close()        # close MySql/SQLite3 connection
    time.sleep(15)      # wait before looking up alarms again

# unused code, ignore
#for dir in os.listdir(lib):
#    if(artist in dir):
#        os.system("gpio write 21 0")
#        os.system("gpio write 7 1")
#        os.system("./player_artist.sh '" + lib + dir + "/'")
#        os.system("gpio write 7 0")
#        os.system("gpio write 21 1")