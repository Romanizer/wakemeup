const express = require('express');
const app = express();
const { readFile } = require('fs').promises;
const dgram = require('dgram');

app.get('/', async (request, response) => {

    response.send( await readFile('./web/index.html', 'utf8') );

});

app.post('/', async (request, response) => {

    console.log("Jake was here!");

    var client = dgram.createSocket('udp4');
    client.send('STOPALARMS', 8888, '192.168.0.9', function(err, bytes) {
        client.close();
    });

});

app.listen(process.env.PORT || 8080, () => console.log(`Listening on http://localhost:8080/`))
