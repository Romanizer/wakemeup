import subprocess
import os
import time

class mplayer:
    
    def __init__(self, mute: int, mute_inv: bool, shutdown: int, shutdown_inv: bool):
        # init gpios here
        self.mute = mute
        self.mute_inv = mute_inv
        self.shutdown = shutdown
        self.shutdown_inv = shutdown_inv
        os.system(f"gpio mode {self.mute} out")
        os.system(f"gpio mode {self.shutdown} out")
        self.player = 0

    def start(self, path):
        if self.running():
            return
        # unmute amp ic
        os.system(f"gpio write {self.shutdown} {int(not self.shutdown_inv)}")
        time.sleep(0.3)
        os.system(f"gpio write {self.mute} {int(not self.mute_inv)}")
        # start to play music
        self.player = subprocess.Popen(["mplayer", path], stdin=subprocess.PIPE, stderr=subprocess.PIPE)

    def stop(self):
        if self.running():
            # stop music player
            self.player.kill()
        # mute amp ic
        os.system(f"gpio write {self.mute} {int(self.mute_inv)}")
        time.sleep(0.3)
        os.system(f"gpio write {self.shutdown} {int(self.shutdown_inv)}")

    def running(self):
        if self.player == 0:
            return False
        return self.player.poll() == None