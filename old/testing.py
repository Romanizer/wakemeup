#import pymysql
#import sqlite3
from subprocess import call
from datetime import datetime, time
import time
from time import strftime, gmtime
import os
#import pytz
#from pytz import timezone
import logging
import random
import subprocess
from configparser import ConfigParser

import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
host = "192.168.0.9"
s.sendto('A'.encode() , (host, 8888))


exit()

config = ConfigParser()
config['settings'] = {
    'timezone': 'Europe/Berlin',
    'usingsqlite': 'yes',
    'waketime': f'{60*50}' # in seconds
}
config['paths'] = {
    'remote': '/media/media/Musik/',
    'local': '/home/goal/wake/local_audio',
    'wakelines':'/home/goal/wake/wakelines/',
}
config['gpio'] = {
    'mute': '7',
    'mute_inv': 'no',
    'shutdown': '21',
    'shutdown_inv': 'yes'
}
config['database'] = {
    'db_addr': '',      # localhost / 192.168.x.x
    'db_user': '',      # your db user, the less priviledges the better
    'db_pass': '',
    'db_name': 'wake',  # name of database 
    'db_tabl': 'wake',
    'db_file': 'wake.db'
}

with open("config.ini", 'w') as f:
    config.write(f)

exit()


# setup gpio
# MUTE PIN
os.system("sudo gpio mode 7 out")
os.system("sudo gpio write 7 1")
# SHUTDOWN PIN
os.system("sudo gpio mode 21 out")
os.system("sudo gpio write 21 0")



player = subprocess.Popen(["mplayer", "local_audio/honey.flac"], stdin=subprocess.PIPE, stderr=subprocess.PIPE)
time.sleep(4)
if player.poll() is None:
    print("process still running")
else:
    print("not running")

print(f"\n\n{player.poll()}\n\n")

time.sleep(1)
#player.stdin.write(b"q")
player.kill()
print("\n\n")
time.sleep(4)
print(player.poll())

if player.poll() is None:
    print("process still running")
else:
    print("not running")